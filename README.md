# About the visualization

## Messaging. What is the message you are trying to communicate with the narrative visualization?

Over the last few decades, automotive manufacturers have made huge leaps in
average fuel economy. In fact, average fuel economy remained essentially static
from 1990 to 2010. Then electric vehicles entered the scene and dramatically
altered the manufacturing landscape. Average industry fuel economy increased
suddenly by 5 MPG compared to the last two decades. This all happened at the
same time that the industry saw even more super cars with incredibly high
horsepower marks.

## Narrative Structure. Which structure was your narrative visualization designed to follow (martini glass, interactive slide show or drop-down story)? How does your narrative visualization follow that structure? (All of these structures can include the opportunity to "drill-down" and explore. The difference is where that opportunity happens in the structure.)

This narrative visualization has used an interactive slide show structure to
present the information. I have a scene per page with a button at the top to get
from page to page. There's a preferred order for the reader to follow. Each scene
contains tooltips for the reader to use for exploring the data more in depth.

## Visual Structure. What visual structure is used for each scene? How does it ensure the viewer can understand the data and navigate the scene? How does it highlight to urge the viewer to focus on the important parts of the data in each scene? How does it help the viewer transition to other scenes, to understand how the data connects to the data in other scenes?

I've used the same structure for each scene. It consists of a scatterplot of the
automobile data with the x-axis mapped to horsepower and the y-axis to Highway
MPG, which I have generalized in this discussion to just MPG. Each point is
colored based on its cylinder count. I also include a single annotation and some
general description to ensure the correct area is highlighted for quick scanning
and additional details are pointed out for those wanting to take more time
reviewing the data.

The description at the top sets the stage for the graph, while the annotation on
the graph highlights the most important aspect of the graph. Each scene uses the
same data and layout, which allows the user to easily transition from scene to
scene without losing focus or becoming disoriented.

## Scenes. What are the scenes of your narrative visualization? How are the scenes ordered, and why

The first scene is a view of all the data, where I start the discussion by
showing the electric vehicle outliers. The next scene is for just the data from
1990 to 1999, which shows that there were no electric vehicles at the time. It
also gives the average industry MPG for the cars in the dataset. The third scene
shows data from 2000 to 2009. This scene also doesn't include any electric
vehicles, and the industry average MPG actually decreases slightly. Once we get
to the fourth scene, data from 2010 to 2017, we can now see the electric
vehicles again. The average industry MPG has also increased 5 MPG, but there are
also even more supercars with higher horsepower engines than in the previous
decade. The fifth scene is all of the data again so the reader can review the
data more carefully and to reiterate the original premise.

## Annotations. What template was followed for the annotations, and why that template? How are the annotations used to support the messaging? Do the annotations change within a single scene, and if so, how and why

The template I used for the static annotations was a brief statement to
highlight the data followed by the industry average MPG. For the tooltip
annotations, I used a format that listed the year, make, and model on the first
line, x and y values on the second line, and the cylinder count on the third
line. I used the static annotations template because it contained just the main
point and the main data element I wanted to take from scene to scene. I used the
tooltip template because I wanted to give the reader enough information to
understand what any individual point meant. The annotations don't change within
a single scene.

## Parameters. What are the parameters of the narrative visualization? What are the states of the narrative visualization? How are the parameters used to define the state and each scene?

My parameters are year, highway MPG, cylinders, and horsepower. The
visualization starts by mapping highway MPG to horsepower in a scatterplot for
each automobile. These points are color-coded by cylinder count. This same
combination is taken through to the other scenes. The next three scenes cover
the last 30 years of automobiles in decade chunks. The final scene brings all
the data points back again.

## Triggers. What are the triggers that connect user actions to changes of state in the narrative visualization? What affordances are provided to the user to communicate to them what options are available to them in the narrative visualization?

The primary triggers are the buttons at the top. When a user clicks a button,
they are taken to the next scene. The other trigger is a mouseover on any of the
data points in the scatterplot which reveals a tooltip at the bottom with
additional information about that point. There aren't any affordances aside from
the text describing what the user can do. It's quite common to have a mouseover
on a data point, so I didn't think there would need to be an affordance. As for
switching scenes, the buttons are quite clear as to what they do and the
ordering of them.

## Data

https://www.kaggle.com/CooperUnion/cardataset/version/1
